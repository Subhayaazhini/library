// import { AuthModule } from './../auth/auth.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AddUsersComponent } from './components/add-users/add-users.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewUsersComponent } from './components/view-users/view-users.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
        ReactiveFormsModule,
        // AuthModule

  ],
  exports:[
    AddUsersComponent
  ],
  declarations: [DashboardComponent, AddUsersComponent, ViewUsersComponent]
})
export class UsersModule { }
