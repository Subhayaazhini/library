import { AddserviceService } from './../../../addservice.service';
import { Component, OnInit,Input } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.css']
})
export class AddUsersComponent implements OnInit {
  
   title = 'Add Users';
   angForm: FormGroup;
  constructor(private addservice: AddserviceService, private fb: FormBuilder, private router: Router) {
    this.createForm();
       }
         createForm() {
    this.angForm = this.fb.group({
      mail: ['', Validators.required ],
      pass: ['', Validators.required ]
   });
  }
  addCoin(mail, pass) {
      this.addservice.addCoin(mail,pass);
      console.log(mail);
  }

  ngOnInit() {
  }

}


// import { Component, OnInit } from '@angular/core';
// import { CoinService } from '../../coin.service';
// import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

// @Component({
//   selector: 'app-create',
//   templateUrl: './create.component.html',
//   styleUrls: ['./create.component.css']
// })
// export class CreateComponent implements OnInit {

//   title = 'Add Coin';
//   angForm: FormGroup;
//   constructor(private coinservice: CoinService, private fb: FormBuilder) {
//     this.createForm();
//    }
//   createForm() {
//     this.angForm = this.fb.group({
//       name: ['', Validators.required ],
//       price: ['', Validators.required ]
//    });
//   }
//   addCoin(name, price) {
//       this.coinservice.addCoin(name, price);
//   }
//   ngOnInit() {
//   }
// }
