import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBooksComponent } from './components/add-books/add-books.component';
import { ViewBooksComponent } from './components/view-books/view-books.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AddBooksComponent, ViewBooksComponent]
})
export class BooksModule { }
