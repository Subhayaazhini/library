import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddserviceService {

  result: any;
  constructor(private http: HttpClient) {}

  addCoin(mail, pass) {
    const uri = 'http://localhost:4000/Members/add';
    const obj = {
      mail: mail,
      pass: pass
    };
    this
      .http
      .post(uri, obj)
      .subscribe(res =>
          console.log('Done'));
  }

  getCoins() {
    const uri = 'http://localhost:4000/Members';
    return this
            .http
            .get(uri)
            .pipe(
              map(res => {
                return res;
              })
            );
  }

  editCoin(id) {
    const uri = 'http://localhost:4000/Members/edit/' + id;
    return this
            .http
            .get(uri)
            .pipe(
              map(res => {
                return res;
              })
            );
  }

  updateCoin(mail, pass, id) {
    const uri = 'http://localhost:4000/Members/update/' + id;
    const obj = {
      mail: mail,
      pass: pass
    };
    this
      .http
      .post(uri, obj)
      .subscribe(res => console.log('Done'));
  }

  deleteCoin(id) {
    const uri = 'http://localhost:4000/Members/delete/' + id;

        return this
            .http
            .get(uri)
            .pipe(
              map(res => {
                return res;
              })
            );
  }
}


