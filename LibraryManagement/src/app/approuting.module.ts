import { AddUsersComponent } from './users/components/add-users/add-users.component';
import { DashboardComponent } from './users/components/dashboard/dashboard.component';
import { LoginComponent } from './auth/components/login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{RouterModule,Routes} from '@angular/router';

const routes:Routes=[
  {
    path:'login',
    component:LoginComponent
     },
  {
  path:'dashboard',
  component:DashboardComponent
   },
   {
    path:'addusers',
    component:AddUsersComponent
     },
  {
    path:'',
    redirectTo:'/login',
    pathMatch:'full'
  },
  
]


@NgModule({
  imports: [
    CommonModule,
     RouterModule.forRoot(routes),
  ],
  exports:[
     RouterModule
  ],
  declarations: []
})
export class ApproutingModule { }

// import { LoginComponent } from './auth/component/login/login.component';
// import { ProductDetailsComponent } from './product/component/product-details/product-details.component';
// import { NgModule } from '@angular/core';
// import{RouterModule,Routes} from '@angular/router';
// import { ProductListComponent } from './product/component/product-list/product-list.component';
// import { CartListComponent } from './cart/component/cart-list/cart-list.component';

// const routes:Routes=[
//   {
//     path:'login',
//     component:LoginComponent
//      },
//   {
//   path:'products',
//   component:ProductListComponent
//    },
//   {
//     path:'cart',
//     component:CartListComponent
//   },
//   {
//     path:'',
//     redirectTo:'/login',
//     pathMatch:'full'
//   },
//   {
//     path:'product-details/:productId',
//     component:ProductDetailsComponent

    
//   }
// ]

// @NgModule({
//   imports: [
//     RouterModule.forRoot(routes),  
    

//   ],
//   exports:[
//     RouterModule,
//   ],
//   declarations: []
// })
// export class AppRoutingModule { }
