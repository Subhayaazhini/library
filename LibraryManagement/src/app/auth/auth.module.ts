import { UsersModule } from './../users/users.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LoginComponent } from './components/login/login.component';

@NgModule({
  imports: [
UsersModule,
CommonModule, AngularFontAwesomeModule,
  ],
  declarations: [LoginComponent],
  exports:[
    LoginComponent
  ]
})
export class AuthModule { }
