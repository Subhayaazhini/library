import { AddserviceService } from './../../../addservice.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 users;
 user;
  constructor(private router:Router, private service:AddserviceService) { this.getdata()}
  
  ngOnInit() {
    
  }
  dashboard(){
    this.router.navigate(['/dashboard']);
  }

  getdata() {
    this.service.getCoins().subscribe(res => {
      this.users = res;
    });
  }

  

  // public loginchangeEvent : Subject<string> = new Subject<string>();


   login(a,b):boolean{
    

     const res = this.users.find((u)=>{
       return u.mail === a && u.pass === b;
     });
     

     if(res){
     sessionStorage.setItem("userEmail",this.users.mail);
     this.router.navigate(['/dashboard']);
    
     return true;
        
     }
     console.log("Invalid User details")
     return false;
   }
    isLoggedIn(){
     
    const userEmail = sessionStorage.getItem('userEmail')
    return userEmail ? true:false;
    }
    logout(){
    sessionStorage.clear();
      
    }

}
